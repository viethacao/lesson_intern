/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export const scope = 'boilerplate.components.Header';

export default defineMessages({
  home: {
    id: `${scope}.home`,
    defaultMessage: 'Home',
  },
  features: {
    id: `${scope}.features`,
    defaultMessage: 'Features',
  },
  imagesaga: {
    id: `${scope}.imagesaga`,
    defaultMessage: 'Imagesaga',
  },
  todolist: {
    id: `${scope}.todolist`,
    defaultMessage: 'Todolist',
  },
  calculator: {
    id: `${scope}.calculator`,
    defaultMessage: 'Calculator',
  },
  counter: {
    id: `${scope}.counter`,
    defaultMessage: 'Counter',
  },
});
