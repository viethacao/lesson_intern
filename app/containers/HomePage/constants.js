/*
 * HomeConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const CHANGE_USERNAME = 'boilerplate/Home/CHANGE_USERNAME';

export const CHANGE_COUNT = 'boilerplate/Home/CHANGE_COUNT';

export const ADD_ITEM = 'boilerplate/Home/ADD_ITEM';

export const DELETE_TODO = 'boilerplate/Home/DELETE_TODO';

export const CHECK_TODO = 'boilerplate/Home/CHECK_TODO';

export const FILTER_TODO = 'boilerplate/Home/FILTER_TODO';

export const EDIT_TODO = 'boilerplate/Home/EDIT_TODO';

export const TOOGLE_ITEM = 'boilerplate/Home/TOOGLE_ITEM';

export const CLEAR_COMPLETED = 'boilerplate/Home/CLEAR_COMPLETED';

export const CALL_API = 'boilerplate/Home/CALL_API';

export const RESPON = 'boilerplate/Home/RESPON';
