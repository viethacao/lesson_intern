import {
  selectHome,
  makeSelectUsername,
  makeSelectList,
  nameSelector,
  responSelector,
} from '../selectors';

describe('selectHome', () => {
  it('should select the home state', () => {
    const homeState = {
      userData: {},
    };
    const mockedState = {
      home: homeState,
    };
    expect(selectHome(mockedState)).toEqual(homeState);
  });
});

describe('makeSelectUsername', () => {
  const usernameSelector = makeSelectUsername();
  it('should select the username', () => {
    const username = 'mxstbr';
    const mockedState = {
      home: {
        username,
      },
    };
    expect(usernameSelector(mockedState)).toEqual(username);
  });
});

describe('makeSelectList', () => {
  const listSelector = makeSelectList();
  it('should select the username', () => {
    const list = {
      id: 1,
      name: '123',
      isCompleted: false,
    };
    const mockedState = {
      home: {
        list,
      },
    };
    expect(listSelector(mockedState)).toEqual(list);
  });
});

describe('nameSelector', () => {
  const nameSel = nameSelector();
  it('should select the username', () => {
    const name = 'ACTIVE_TODO';
    const mockedState = {
      home: {
        name,
      },
    };
    expect(nameSel(mockedState)).toEqual(name);
  });
});

describe('responSelector', () => {
  const imageSelector = responSelector();
  it('should select the username', () => {
    const image = '123';
    const mockedState = {
      home: {
        image,
      },
    };
    expect(imageSelector(mockedState)).toEqual(image);
  });
});
