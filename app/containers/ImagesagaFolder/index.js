import React from 'react';
import { createStructuredSelector } from 'reselect';
import { useSelector, useDispatch } from 'react-redux';
import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';
import {
  makeSelectRepos,
  makeSelectLoading,
  makeSelectError,
} from 'containers/App/selectors';

import reducer from 'containers/HomePage/reducer';
import { callAPI } from 'containers/HomePage/actions';
import {
  makeSelectUsername,
  makeSelectList,
  nameSelector,
  responSelector,
} from 'containers/HomePage/selectors';
import saga from 'containers/HomePage/saga';

const key = 'home';

const stateSelector = createStructuredSelector({
  repos: makeSelectRepos(),
  username: makeSelectUsername(),
  loading: makeSelectLoading(),
  error: makeSelectError(),
  list: makeSelectList(),
  name: nameSelector(),
  image: responSelector(),
});

export default function Imagesaga() {
  const { image } = useSelector(stateSelector);

  const dispatch = useDispatch();

  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  const getAPI = () => {
    dispatch(callAPI());
  };

  return (
    <div>
      <button type="button" onClick={getAPI}>
        saga
      </button>
      <img src={image} alt="123" />
    </div>
  );
}
