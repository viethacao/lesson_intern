import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Calculator from '../index';

describe('<Calculator />', () => {
  it('defined render', () => {
    const { container } = render(<Calculator />);
    expect(container).toBeDefined();
  });

  it('event onclick', () => {
    const { container } = render(<Calculator />);
    const number = container.getElementsByClassName('number');
    number.forEach(element => {
      fireEvent.click(element);
      expect(container.querySelector('.output-display')).toHaveTextContent(
        element.name,
      );
    });
  });
  it('event onclick operator', () => {
    const { container, getByText } = render(<Calculator />);
    const add = getByText('+');
    const first = getByText('1');
    const second = getByText('2');
    const equal = getByText('=');
    const result = container.querySelector('.calculator');
    fireEvent.click(first);
    fireEvent.click(add);
    fireEvent.click(second);
    fireEvent.click(equal);
    expect(result).toHaveTextContent(3);
  });

  it('event onclick operator', () => {
    const { container, getByText } = render(<Calculator />);
    const sub = getByText('-');
    const first = getByText('1');
    const second = getByText('2');
    const equal = getByText('=');
    const result = container.querySelector('.calculator');
    fireEvent.click(second);
    fireEvent.click(sub);
    fireEvent.click(first);
    fireEvent.click(equal);
    expect(result).toHaveTextContent(1);
  });

  it('event onclick operator', () => {
    const { container, getByText } = render(<Calculator />);
    const multi = getByText('*');
    const first = getByText('1');
    const second = getByText('2');
    const equal = getByText('=');
    const result = container.querySelector('.calculator');
    fireEvent.click(second);
    fireEvent.click(multi);
    fireEvent.click(first);
    fireEvent.click(equal);
    expect(result).toHaveTextContent(2);
  });

  it('event onclick operator', () => {
    const { container, getByText } = render(<Calculator />);
    const divide = getByText('/');
    const first = getByText('1');
    const second = getByText('2');
    const equal = getByText('=');
    const result = container.querySelector('.calculator');
    fireEvent.click(second);
    fireEvent.click(divide);
    fireEvent.click(first);
    fireEvent.click(equal);
    expect(result).toHaveTextContent(2);
  });

  it('event onclick operator', () => {
    const { getByText } = render(<Calculator />);
    const second = getByText('2');
    const equal = getByText('=');
    fireEvent.click(second);
    fireEvent.click(equal);
    fireEvent.click(equal);
  });

  it('event onclick reset', () => {
    const { container, getByText } = render(<Calculator />);
    fireEvent.click(getByText('C'));
    expect(container.querySelector('.calculator')).toHaveTextContent('');
    expect(container.querySelector('.output-display')).toHaveTextContent('');
  });

  it('event onclick remove', () => {
    const { container, getByText } = render(<Calculator />);
    const result = container.querySelector('.output-display');
    const remove = container.querySelector('#remove');
    const first = getByText('1');
    const second = getByText('2');
    fireEvent.click(first);
    fireEvent.click(second);
    fireEvent.click(remove);
    expect(result).toHaveTextContent(1);
  });
});
